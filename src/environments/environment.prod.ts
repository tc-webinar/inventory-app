export const environment = {
  production: true,
  apiBase: 'https://tc-inv-api.herokuapp.com/api/v1'
};
